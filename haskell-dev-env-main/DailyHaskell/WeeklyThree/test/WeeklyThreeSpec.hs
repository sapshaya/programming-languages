module WeeklyThreeSpec where

import Test.Hspec
import WeeklyThree


spec :: Spec
spec = do
    describe "Vec Show instance" $ do
        it "returns the correct string representation for Vec [1.0, 2.0, 3.0, 4.0]" $
            show (Vec [1.0, 2.0, 3.0, 4.0]) `shouldBe` "Vec [1.0,2.0,3.0,4.0]"

    describe "Vec Num instance" $ do
        it "returns the correct result for Vec addition" $
            (Vec [1.0, 2.0, 3.0, 4.0] + Vec [5.0, 6.0, 7.0, 8.0]) `shouldBe` Vec [6.0, 8.0, 10.0, 12.0]

        it "returns the correct result for Vec subtraction" $
            (Vec [1.0, 2.0, 3.0, 4.0] - Vec [5.0, 6.0, 7.0, 8.0]) `shouldBe` Vec [-4.0, -4.0, -4.0, -4.0]

        it "returns the correct result for Vec multiplication" $
            (Vec [1.0, 2.0, 3.0, 4.0] * Vec [5.0, 6.0, 7.0, 8.0]) `shouldBe` Vec [5.0, 12.0, 21.0, 32.0]

        it "returns the correct result for abs (Vec [-1.0, -2.0, -3.0, -4.0])" $
            abs (Vec [-1.0, -2.0, -3.0, -4.0]) `shouldBe` Vec [1.0, 2.0, 3.0, 4.0]

        it "returns the correct result for signum (Vec [1.0, -2.0, 3.0, -4.0])" $
            signum (Vec [1.0, -2.0, 3.0, -4.0]) `shouldBe` Vec [1.0, -1.0, 1.0, -1.0]

        it "returns the correct result for fromInteger 2" $
            (fromInteger 2 :: Vec) `shouldBe` Vec [2.0, 2.0, 2.0, 2.0]

    describe "Vec Eq instance" $ do
        it "returns True for Vec equality" $
            (Vec [1.0, 2.0, 3.0, 4.0] == Vec [1.0, 2.0, 3.0, 4.0]) `shouldBe` True

    describe "Vec Ord instance" $ do
        it "returns LT for compare (Vec [1.0, 2.0, 3.0, 4.0]) (Vec [5.0, 6.0, 7.0, 8.0])" $
            compare (Vec [1.0, 2.0, 3.0, 4.0]) (Vec [5.0, 6.0, 7.0, 8.0]) `shouldBe` LT

        it "returns False for (Vec [1.0, 2.0, 3.0, 4.0]) > (Vec [5.0, 6.0, 7.0, 8.0])" $
            (Vec [1.0, 2.0, 3.0, 4.0] > Vec [5.0, 6.0, 7.0, 8.0]) `shouldBe` False

        it "returns True for (Vec [1.0, 2.0, 3.0, 4.0]) <= (Vec [5.0, 6.0, 7.0, 8.0])" $
            (Vec [1.0, 2.0, 3.0, 4.0] <= Vec [5.0, 6.0, 7.0, 8.0]) `shouldBe` True

        it "returns Vec [5.0, 6.0, 7.0, 8.0] for max (Vec [1.0, 2.0, 3.0, 4.0]) (Vec [5.0, 6.0, 7.0, 8.0])" $
            max (Vec [1.0, 2.0, 3.0, 4.0]) (Vec [5.0, 6.0, 7.0, 8.0]) `shouldBe` Vec [5.0, 6.0, 7.0, 8.0]

    describe "VecT magnitude" $ do
        it "returns the correct result for magnitude (Vec [3.0, 4.0])" $
            magnitude (Vec [3.0, 4.0]) `shouldBe` 5.0

    describe "Vec Semigroup instance" $ do
        it "returns the correct result for Vec concatenation" $
            (Vec [1.0, 2.0] <> Vec [3.0, 4.0]) `shouldBe` Vec [1.0, 2.0, 3.0, 4.0]

    describe "Vec Monoid instance" $ do
        it "returns the correct result for mempty :: Vec" $
            mempty `shouldBe` Vec []
