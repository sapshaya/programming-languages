module WeeklyThree where

-- Q1, Q2
-- Define a new type Vec and make it an instance of Show

data Vec = Vec [Double]
  deriving (Show)


-- Q3
-- Make Vec an instance of Num

instance Num Vec where
  (+) (Vec a) (Vec b) = Vec (zipWith (+) a b)
  (-) (Vec a) (Vec b) = Vec (zipWith (-) a b)
  (*) (Vec a) (Vec b) = Vec (zipWith (*) a b)
  abs (Vec a) = Vec (map abs a)
  signum (Vec a) = Vec (map signum a)
  fromInteger int = Vec (repeat (fromIntegral int))


-- Q4
-- Make Vec an instance of Eq

instance Eq Vec where
  (==) (Vec a) (Vec b) = and (zipWith (==) a b)


-- Q5
-- Make Vec an instance of Ord

instance Ord Vec where
  compare (Vec a) (Vec b) = compare (sum a) (sum b)
  (<) (Vec a) (Vec b) = a < b
  (<=) (Vec a) (Vec b) = a <= b
  (>) (Vec a) (Vec b) = a > b
  (>=) (Vec a) (Vec b) = a >= b
  max (Vec a) (Vec b) = Vec (zipWith max a b)
  min (Vec a) (Vec b) = Vec (zipWith min a b)


-- Q6
-- Create a new typeclass VecT

class VecT a where
  magnitude :: a -> Double


-- Q7
-- Make Vec an instance of VecT

instance VecT Vec where
  magnitude (Vec a) = sqrt (sum (map (\x -> x * x) a))


-- Q8
-- Make Vec an instance of Semigroup

instance Semigroup Vec where
  (<>) (Vec a) (Vec b) = Vec (a <> b)


-- Q9
-- Make Vec an instance of Monoid

instance Monoid Vec where
  mempty = Vec []
