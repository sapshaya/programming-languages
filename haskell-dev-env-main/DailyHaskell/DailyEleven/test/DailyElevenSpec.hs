module DailyElevenSpec where

import Test.Hspec
import DailyEleven

spec :: Spec
spec = do
  describe "allLefts" $ do
    it "returns an empty list when given an empty list" $
      allLefts [] `shouldBe` ([] :: [Int])

    it "returns a list of left values" $
      allLefts [Left "hello", Right 42, Left "world"] `shouldBe` ["hello", "world"]

  describe "produceStringOrSum" $ do
    it "returns the left value if either argument is a left value" $
      produceStringOrSum (Left "error") (Right 10) `shouldBe` Left "error"

    it "returns the sum of the right values if both arguments are right values" $
      produceStringOrSum (Right 5) (Right 7) `shouldBe` Right 12

  describe "sumListOfEither" $ do
    it "returns 0 when given an empty list" $
      sumListOfEither [] `shouldBe` Right 0

    it "returns the left value if any element in the list is a left value" $
      sumListOfEither [Right 1, Left "error", Right 2] `shouldBe` Left "error"

    it "returns the sum of the right values in the list" $
      sumListOfEither [Right 3, Right 4, Right 5] `shouldBe` Right 12
