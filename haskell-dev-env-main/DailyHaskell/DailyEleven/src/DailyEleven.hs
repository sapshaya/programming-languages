module DailyEleven where


    --gets all left values from a list of eithers
    --  consumes a list of eithers
    --  produces a list of the left type
    allLefts :: [Either a b] -> [a]
    allLefts [] = []
    allLefts (x:xs) = case x of
                        (Left a) -> a : allLefts xs
                        (Right _) -> allLefts xs


    --produces either a string or a sum of integers
    --  consumes two eithers of left string and right integer
    --  produces an either of left string and right integer
    produceStringOrSum :: (Either String Integer) -> (Either String Integer) -> (Either String Integer)
    produceStringOrSum (Left s) _ = Left s
    produceStringOrSum _ (Left s) = Left s
    produceStringOrSum (Right x) (Right y) = Right (x+y)


    sumListOfEither :: [Either String Integer] -> (Either String Integer)
    sumListOfEither [] = Right 0
    sumListOfEither (x:xs) = case x of
                                  Left s -> Left s
                                  Right n -> let next = sumListOfEither xs in case next of
                                             Left _ -> sumListOfEither xs
                                             Right i -> Right (n+i)