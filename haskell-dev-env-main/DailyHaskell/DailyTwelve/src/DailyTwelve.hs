module DailyTwelve where


-- Determines if a function, mapped on a list, returns only 'Nothing' values.
--   Takes a function and a list as input.
--   Returns a boolean indicating whether all the function outputs are 'Nothing'.
onlyNothing :: (a -> Maybe b) -> [a] -> Bool
onlyNothing _ [] = True
onlyNothing f (x:xs) = case f x of
    Nothing -> onlyNothing f xs
    Just _ -> False

-- Finds the first non-'Nothing' answer from a function mapped to a list.
--   Takes a function and a list as input.
--   Returns a 'Maybe' type representing the output of the function.
firstAnswer :: (a -> Maybe b) -> [a] -> Maybe b
firstAnswer _ [] = Nothing
firstAnswer f (x:xs) = case f x of
    Nothing -> firstAnswer f xs
    Just b -> Just b

-- Determines all the answers from a function mapped to a list that can have multiple answers.
--   Takes a function and a list as input.
--   Returns a 'Maybe' type representing a list of the function outputs.
allAnswers :: (a -> Maybe [b]) -> [a] -> Maybe [b]
allAnswers _ [] = Just []
allAnswers f (x:xs) = case f x of
    Nothing -> Nothing
    Just _ -> helper (f x) (allAnswers f xs)

-- Helper method for 'allAnswers'.
--   Concatenates 'Maybe' lists.
helper :: Maybe [b] -> Maybe [b] -> Maybe [b]
helper a b = case a of
    Nothing -> Nothing
    Just as -> case b of
        Nothing -> Nothing
        Just bs -> Just (as ++ bs)
