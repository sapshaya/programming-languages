module DailyTwelveSpec where

import Test.Hspec
import DailyTwelve

spec :: Spec
spec = do
    describe "onlyNothing" $ do
        it "onlyNothing 1" $
            onlyNothing (const Nothing) [5,6,7,8] `shouldBe` True
        it "onlyNothing 2" $
            onlyNothing (\x -> Just (x+2)) [5,6,7,8] `shouldBe` False
        it "onlyNothing 3" $
            onlyNothing (\x -> if even x then Nothing else Just x) [9876,10,14,15,3,16,124,8,6,13,567,0] `shouldBe` False

    describe "firstAnswer" $ do
        it "firstAnswer 1" $
            firstAnswer (\x -> if x `mod` 1 == 0 then Nothing else Just x) [1,2,3,4] `shouldBe` Nothing
        it "firstAnswer 2" $
            firstAnswer (\x -> Just (x+1)) [1,2,3,4] `shouldBe` Just 2
        it "firstAnswer 3" $
            firstAnswer (\x -> if even x then Nothing else Just x) [9876,10,14,15,3,16,124,8,6,13,567,0] `shouldBe` Just 15

    describe "allAnswers" $ do
        it "allAnswers 1" $
            allAnswers (\x -> if x `mod` 4 == 0 then Nothing else Just [x]) [5,6,7,8] `shouldBe` Nothing
        it "allAnswers 2" $
            allAnswers (\x -> Just [x, x+2]) [5,6,7,8] `shouldBe` Just [5,7,6,8,7,9,8,10]
        it "allAnswers 3" $
            allAnswers (\x -> if x `mod` 4 == 0 then Nothing else Just [x, x `mod` 4]) [9876,10,14,15,3,16,124,8,6,13,567,0] `shouldBe` Nothing
