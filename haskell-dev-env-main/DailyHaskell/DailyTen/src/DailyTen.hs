module DailyTen where

import Data.Char (isAlpha)
import Data.List (sort)

-- firstFunctorLaw: Function to check the first Functor law.
firstFunctorLaw :: (Eq (f a), Functor f) => f a -> Bool
firstFunctorLaw f = (fmap id f == id f)

-- secondFunctorLaw: Function to check the second Functor law.
secondFunctorLaw :: (Eq (f c), Functor f) => (b -> c) -> (a -> b) -> f a -> Bool
secondFunctorLaw f1 f2 f = (fmap (f1 . f2) f == fmap f1 (fmap f2 f))

-- Helper methods for testing.
-- addFive: Function that adds five to a Maybe Integer value.
addFive :: Maybe Integer -> Maybe Integer
addFive (Just a) = Just (a + 5)
addFive Nothing = Nothing

-- multTen: Function that multiplies a Maybe Integer value by ten.
multTen :: Maybe Integer -> Maybe Integer
multTen (Just a) = Just (a * 10)
multTen Nothing = Nothing