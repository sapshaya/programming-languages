module DailyTenSpec where

import Test.Hspec
import DailyTen

import Data.Char ( chr, isAlpha )
spec :: Spec
spec = do
    describe "firstFunctorLaw" $ do
        it "firstFunctorLaw 1" $
            (firstFunctorLaw (Just ('c', 35))) `shouldBe` True
        it "firstFunctorLaw 2" $
            (firstFunctorLaw [2,3,5,7,11]) `shouldBe` True
    describe "secondFunctorLaw" $ do
        it "secondFunctorLaw 1" $
            (secondFunctorLaw isAlpha fst (Just ('c', 35))) `shouldBe` True
        it "secondFunctorLaw 2" $
            (secondFunctorLaw chr (+96) [2,3,5,7,11]) `shouldBe` True
    describe "Either functor laws" $ do
        it "firstFunctorLaw 1" $
            (firstFunctorLaw ((Left (Just 10)) :: Either (Maybe Integer) (Maybe Integer))) `shouldBe` True
        it "firstFunctorLaw 2" $
            (firstFunctorLaw ((Right Nothing) :: Either (Maybe Integer) (Maybe Integer))) `shouldBe` True
        it "firstFunctorLaw 3" $
            (firstFunctorLaw ((Right (Just 35)) :: Either (Maybe Integer) (Maybe Integer))) `shouldBe` True
        it "secondFunctorLaw 1" $
            (secondFunctorLaw addFive multTen ((Left (Just 7)) :: Either (Maybe Integer) (Maybe Integer))) `shouldBe` True
        it "secondFunctorLaw 2" $
            (secondFunctorLaw addFive multTen ((Right Nothing) :: Either (Maybe Integer) (Maybe Integer))) `shouldBe` True
        it "secondFunctorLaw 3" $
            (secondFunctorLaw addFive multTen ((Right (Just 35)) :: Either (Maybe Integer) (Maybe Integer))) `shouldBe` True