module DailyFourSpec where

import Test.Hspec
import DailyFour

spec :: Spec  
spec = do 
    describe "mergeSorted3" $ do
        it "merge sorts [3] [2] [1] into a new list" $
            mergeSorted3 [3] [2] [1] `shouldBe` [1,2,3]
        it "merge sorts [1,2,3] [0,14,11] [-1,-2,-3] into a new list" $
            mergeSorted3 [1,2,3] [0,14,11] [-1,-2,-3] `shouldBe` [-3,-2,-1,0,1,2,3,11,14]
        it "merge sorts [10,15,18] [2,11,13] [1,14,16] into a new list" $
            mergeSorted3 [10,15,18] [2,11,13] [1,14,16] `shouldBe` [1,2,10,11,13,14,15,16]
    
    describe "zip3Lists" $ do
        it " zips [1,2,3] ['a','b','c'] [4,5,6] together" $
            zip3Lists [1,2,3] ['a','b','c'] [4,5,6] `shouldBe` [(1,'a',4),(2,'b',5),(3,'c',6)]
        it " zips [1,2,3] [4,5,6] [7,8,9] together" $
            zip3Lists [1,2,3] [4,5,6] [7,8,9] `shouldBe` [(1,4,7),(2,5,8),(3,6,9)]
        it " zips ['a','b','c'] ['a','a','a'] ['r','g','h'] together" $
            zip3Lists ['a','b','c'] ['a','a','a'] ['r','g','h'] `shouldBe` [('a','a','r'),('b','a','g'),('c','a','h')]
    
    describe "unzipTriples" $ do
        it "unzips [(1,'a',4),(2,'b',5),(3,'c',6)]" $
            unzipTriples [(1,'a',4),(2,'b',5),(3,'c',6)] `shouldBe` ([1,2,3],['a','b','c'], [4,5,6])
        it "unzips [(-1,100,5),(0,3,5),(5,2,5),(1,2,3)]" $
            unzipTriples [(-1,100,5),(0,3,5),(5,2,5),(1,2,3)] `shouldBe` ([-1,0,5,1],[100,3,2,2], [5,5,5,3])
        it "unzips [('c','a','b'),('b','a','t'),('r','a','g')]" $
            unzipTriples [('c','a','b'),('b','a','t'),('r','a','g')] `shouldBe` (['c','b','r'],['a','a','a'], ['b','t','g'])