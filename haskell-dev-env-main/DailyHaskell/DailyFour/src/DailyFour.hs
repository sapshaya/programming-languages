module DailyFour where

{-
This function takes three lists of different types and it returns a lsit of tuples and it contains
one element from each of input lists. It uses recursion to combine first element of each list to
tuple and after calling itself with the rest of the elements until one or more of the lists are
empty
-}
zip3Lists :: [a] -> [b] -> [c] -> [(a,b,c)]
zip3Lists [] [] [] = []
zip3Lists (x:xs) (y:ys) (z:zs) = 
    (x,y,z) : zip3Lists xs ys zs
{-
This function takes two sorted lists of elements that can be Ord (ordered) and returns a 
single sorted list containing all the elements from both input lists.
-}
mergeSorted2 :: Ord a => [a] -> [a] -> [a]
mergeSorted2 [] [] = []
mergeSorted2 [] xs = xs
mergeSorted2 xs [] = xs
mergeSorted2 (x:xs) (y:ys) =
    if x <= y 
    then x : mergeSorted2 xs (y:ys)
    else y : mergeSorted2 (x:xs) ys
{-
This function takes three sorted lists and returns a single sorted list that contains all the 
elements from all three input lists. 
-}
mergeSorted3 :: Ord a => [a] -> [a] -> [a] -> [a]
mergeSorted3 [] [] [] = []
mergeSorted3 xs [] [] = xs
mergeSorted3 [] ys [] = ys
mergeSorted3 [] [] zs = zs

mergeSorted3 [] ys zs =
    mergeSorted2 ys zs
mergeSorted3 xs [] zs =
    mergeSorted2 xs zs
mergeSorted3 xs ys [] =
    mergeSorted2 xs ys
mergeSorted3 xs ys zs = 
    mergeSorted2 xs sndHalfSorted
    where
        sndHalfSorted = mergeSorted2 ys zs
{-
This function takes a list of triples and returns a tuple of three lists, 
each containing the elements from the corresponding position in the input triples.
-}
unzipTriples :: [(a1, b, a2)] -> ([a1],[b], [a2])
unzipTriples [] = ([], [], [])
unzipTriples xs = 
    (unzipFirst xs, unzipSecond xs, unzipThird xs)
{-
This function takes a list of triples and returns a list of the first elements in each triple.
-}
unzipFirst :: [(a,b,c)] -> [a]
unzipFirst [] = []
unzipFirst (x:xs) =
    (first x) : unzipFirst xs
    where
        first (a,_,_) = a 
{-
This function takes a list of triples and returns a list of the second elements in each triple.
-}
unzipSecond :: [(a1,a2,c)] -> [a2]
unzipSecond [] = []
unzipSecond (x:xs) = 
    (second x) : unzipSecond xs
    where
        second (_,b,_) = b 
{-
This function takes a list of triples and returns a list of the third elements in each triple.
-}
unzipThird :: [(a1,b,a2)] -> [a2]
unzipThird [] = []
unzipThird (x:xs) = 
    (third x) : unzipThird xs 
    where 
        third (_,_,c) = c
    