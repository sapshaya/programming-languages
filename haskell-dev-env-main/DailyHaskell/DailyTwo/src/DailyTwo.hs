module DailyTwo where
{-
This function takes a list and returns a new list containing every fourth element of the original list. 
If the list has fewer than four elements, an empty list is returned. 
-}
every4th :: [a] -> [a]
every4th (fst:snd:thrd:frth:rest) =frth : every4th rest
every4th _ = []
{-
This function takes two lists of fractional numbers and returns the sum of the two lists. 
A Fractional representing the dot quotient of the two given lists
The function returns 0 if both lists are empty.
-}
tupleDotQuotient :: Fractional p => [p] -> [p] -> p 
tupleDotQuotient [] [] = 0
tupleDotQuotient (x:xs) (y:ys) = 
    x / y + tupleDotQuotient xs ys
{-
This function takes a string and a list of strings, 
and returns a new list of strings where the first list is appended to each element of the input list of strings. 
-}
appendToEach :: [a] -> [[a]] -> [[a]]
appendToEach str [] = []
appendToEach str (x:xs) = 
    (x ++ str) : (appendToEach str xs)
{-
This function removes duplicates values from a list of values and as a result a list in which
every original element appears at most one time

-}
toSetList :: Eq a => [a] -> [a]
toSetList [] = []
toSetList (x:xs) = x : toSetList (filter (/= x) xs)