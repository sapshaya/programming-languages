module DailyTwoSpec where

import Test.Hspec
import DailyTwo
--import Distribution.Simple.Program.HcPkg (describe)
--import DailyTwo (every4th, tupleDotQuotient, appendToEach)
spec :: Spec 
spec = do 
    describe "every4th" $ do
        it "returns every 4th element in [1,1,1,1,1]" $
            every4th [1,1,1,1,1] `shouldBe` [1]
        it "returns every 4th element in [1...10]" $
            every4th [1..10] `shouldBe` [4,8]
        it "returns every 4th element in [1,3,4,6,9,10,12,15]" $
            every4th [1,3,4,6,9,10,12,15] `shouldBe` [6,15]
    
    describe "tupleDotQuotient" $ do
        it "returns the tupleDotQuotient of [1,1,1] and [1,1,1]" $
            tupleDotQuotient [1,1,1] [1,1,1] `shouldBe` 3
        it "returns the tupleDotQuotient of [] and []" $
            tupleDotQuotient [2,4,6] [2,2,2] `shouldBe` 6
        it "returns the tupleDotQuotient of [-10,-5,-1] and [2,-5,1]" $
            tupleDotQuotient [-10,-5,-1] [2,-5,1] `shouldBe` -5

    describe "appendToEach" $ do
        it "appends \"!\" to [\"Hey\", \"Bye\"]" $
            appendToEach "!" ["Hey", "Bye"] `shouldBe` ["Hey!", "Bye!"]
        it "appends \" no\" to [\"Please\", \"Yes but\"]" $
            appendToEach " no" ["Please", "Yes but"] `shouldBe` ["Please no", "Yes but no"]
        it "appends \"Mall\" to [\"Big \", \"Amazing \"]" $
            appendToEach "Mall" ["Big ", "Amazing "] `shouldBe` ["Big Mall", "Amazing Mall"]
    
    describe "toSetList" $ do
        it "removes duplicates from [1,2,3,1,2,3]" $
            toSetList [1,2,3,1,2,3] `shouldBe` [1,2,3]
        it "removes duplicates from [0,0,0,-1,0]" $
            toSetList [0,0,0,1,0] `shouldBe` [0,1]
        it "removes duplicates from [6,6,4,6,1,15,145,1]" $
            toSetList [6,6,4,6,1,15,145,1] `shouldBe` [6,4,15,145]
        
        