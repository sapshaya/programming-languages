module WeeklyOneSpec where

import Test.Hspec
import WeeklyOne

spec :: Spec
spec = do
    describe "removeChar" $ do
        it "removes 'e' from \"super set\"" $
            removeChar 'e' "super set" `shouldBe` "supr st"
        it "removes ' ' from \"super set\"" $
            removeChar ' ' "super set" `shouldBe` "superset"
        it "removes ':' from \"hi: bye\"" $
            removeChar ':' "hi: bye" `shouldBe` "hi bye"
    
    describe "removeWhiteSpace" $ do
        it "removes the white space from \"\n how are  you \r\"" $
            removeWhiteSpace "\n  how are  you \r" `shouldBe` "howareyou"
        it "removes the white space from \"No, i do   want this\"" $
            removeWhiteSpace "No, i do   want this" `shouldBe` "No,idowantthis"
    
    describe "removePunctuation" $ do
        it "removes the punctuation from \"Hey (How are you;)\"" $
            removePunctuation "Hey (How are you;)" `shouldBe` "Hey How are you"
        it "removes the punctuation from \".,[{()]}..,\"" $
            removePunctuation ".,[{()]}..," `shouldBe` ""
        it "removes the punctuation from \"[{hi hi hi}]\"" $
            removePunctuation "[{hi hi hi}]" `shouldBe` "hi hi hi"
    
    describe "charsToAscii" $ do
        it "converts \"Hi! We are happy.\" to a list of ascii values" $
            charsToAscii "Hi! We are happy." `shouldBe` [72,105,33,32,87,101,32,97,114,101,32,104,97,112,112,121,46]
        it "converts \"Hello Hello Hello\" to a list of ascii values" $
            charsToAscii "Hello Hello Hello" `shouldBe` [72,101,108,108,111,32,72,101,108,108,111,32,72,101,108,108,111]
        it "converts \"We are so so so... excited to see you!\" to a list of ascii values" $
            charsToAscii "We are so so so... excited to see you!" `shouldBe` [87,101,32,97,114,101,32,115,111,32,115,111,32,115,111,46,46,46,32,101,120,99,105,116,101,100,32,116,111,32,115,101,101,32,121,111,117,33]
    
    describe "asciiToChars" $ do
        it "converts from ascii to \"Hi! We are happy.\"" $
            asciiToChars [72,105,33,32,87,101,32,97,114,101,32,104,97,112,112,121,46] `shouldBe` "Hi! We are happy."
        it "converts from ascii to \"Hello Hello Hello\"" $
            asciiToChars [72,101,108,108,111,32,72,101,108,108,111,32,72,101,108,108,111] `shouldBe` "Hello Hello Hello"
        it "converts from ascii to \"We are so so so... excited to see you!\"" $
            asciiToChars [87,101,32,97,114,101,32,115,111,32,115,111,32,115,111,46,46,46,32,101,120,99,105,116,101,100,32,116,111,32,115,101,101,32,121,111,117,33] `shouldBe` "We are so so so... excited to see you!"
    
    describe "shiftInts" $ do
        it "shift [1...10] by 1" $
            shiftInts 1 [1..10] `shouldBe` [2..11]
        it "shift [1...10] by 2" $
            shiftInts (-2) [1..10] `shouldBe` [0..8]
        it "shift [99...100] by 99" $
            shiftInts (-99) [99..100] `shouldBe` [0..1]
    
    describe "shiftMessage" $ do
        it "shifts \"This is a secret message\" ascii by 5" $
            shiftMessage 5 "This is a secret message" `shouldBe` "Ymnx%nx%f%xjhwjy%rjxxflj"
        it "shifts \"Ymnx%nx%f%xjhwjy%rjxxflj\" by -5" $
            shiftMessage (-5) "Ymnx%nx%f%xjhwjy%rjxxflj" `shouldBe` "This is a secret message"
        it "shifts \"Test Test Test\" by 100" $
            shiftMessage 100 "Test Test Test" `shouldBe` "8IWX\EOT8IWX\EOT8IWX"
        
        