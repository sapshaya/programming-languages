{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Redundant bracket" #-}
{-# HLINT ignore "Use foldr" #-}
module WeeklyOne where
import Prelude

{-
This function takes a char and a string, then returns that string woth all instances of the char removed.
-}
removeChar :: Eq a => a -> [a] -> [a]
removeChar char [] = []
removeChar char (c:str) = 
    if c == char
    then removeChar char str
    else c : removeChar char str
{-
This function takes a string and returns string with all instances of tabs,spaces, new line characters and carriage
returns removed.
-}
removeWhiteSpace :: [Char] -> [Char]
removeWhiteSpace = (removeChar '\r').(removeChar '\t').(removeChar '\n').(removeChar ' ')
{-
This function takes a string and returns that string with all instances of period,comas, square brackets, parentheses and
curly brackets removed.
-}
removePunctuation :: [Char] -> [Char]
removePunctuation = (removeChar '.').(removeChar ',').(removeChar '(').(removeChar ')').(removeChar '[').(removeChar ']').(removeChar ';').(removeChar ':').(removeChar '?').(removeChar '{').(removeChar '}')
{-
This function takes a char produces a list contain of each char numerical ascii value
-}
charsToAscii :: [Char] -> [Int]
charsToAscii "" = []
charsToAscii (x:xs) =
    fromEnum x : charsToAscii xs
{-
This function takes ascii values in int and produces a char represented by those values
-}
asciiToChars :: [Int] -> [Char]
asciiToChars [] = ""
asciiToChars (x:xs) =
    toEnum x : asciiToChars xs 
{-
This function takes an int shift value and a list of ascii values, 
shifts each value by the shift amount, wrapping around to the beginning 
of the ascii table if necessary, and returns the shifted values as a list using recursion.
-}
shiftInts :: Int -> [Int] -> [Int]
shiftInts shift [] = []
shiftInts shift (x:xs) =
    if (x+shift) >= 128
    then (x+shift) `mod` 128 : shiftInts shift xs 
    else (x+shift) : shiftInts shift xs
{-
This function takes an int shift value and a message string and
converts the string to a list of ascii values, shifts ascii values,
and converts the shifted ascii values back to a string
-}
shiftMessage :: Int -> [Char] -> [Char]
shiftMessage shift "" = ""
shiftMessage shift str = 
    asciiToChars (shiftInts shift (charsToAscii str))
