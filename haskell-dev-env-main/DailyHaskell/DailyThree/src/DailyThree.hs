module DailyThree where
{-
This function will remove everything that is not equal to the given element. It takes an element and a list of elements and returns 
a new list that contains only the elements that match the given element.
-}
removeAllExcept :: Eq a => a -> [a] -> [a]
removeAllExcept e [] = []
removeAllExcept e (x: xs) = 
    if e == x
    then x : removeAllExcept e xs 
    else removeAllExcept e xs 
{-
This function counts how many times a given element occurs in the given list. It takes an element and 
a list of elements and returns the number of times that the element occurs in the list.
-}
countOccurrences :: Eq a => a -> [a] -> Int
countOccurrences e [] = 0
countOccurrences e (x:xs) =
    if e == x 
    then 1 + countOccurrences e xs 
    else countOccurrences e xs
{-
Replace all occurrences of the first argument with the second argument in the list and return the new list. It takes 
two elements and a list of elements and returns 
a new list where every occurrence of the first element is replaced by the second element.
-}
substitute :: Eq a => a -> a -> [a] -> [a]
substitute o r [] = []
substitute o r (x:xs) =
    if x == o 
    then r : substitute o r xs 
    else x : substitute o r xs 