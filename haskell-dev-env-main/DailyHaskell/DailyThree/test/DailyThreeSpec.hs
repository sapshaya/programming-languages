module DailyThreeSpec where

import Test.Hspec
import DailyThree

spec :: Spec  
spec = do
    describe "removeAllExcept" $ do
        it "removes all elements except for 3" $
            removeAllExcept 3 [0,0,3,0,0] `shouldBe` [3]
        it "removes all elements except for 10" $
            removeAllExcept 10 [1..10] `shouldBe` [10]
        it "removes all elements except for -1" $
            removeAllExcept (-1) [] `shouldBe` []
    
    describe "countOccurrences" $ do
        it "counts the occurrences of 3" $
            countOccurrences 3 [1..10] `shouldBe` 1
        it "counts the occurrences of -1" $
            countOccurrences (-1) [-10..10] `shouldBe` 1
        it "counts the occurrences of 99" $
            countOccurrences 11 [] `shouldBe` 0
    
    describe "substitute" $ do
        it "substitutes every 3 with a -3" $
            substitute 3 (-3) [3,3,3,3,3] `shouldBe` [-3,-3,-3,-3,-3]
        it "substitutes every 1 with a 10" $
            substitute 1 10 [1,1,1,10,10] `shouldBe` [10,10,10,10,10]
        it "substitutes every 8 with a 4" $
            substitute 8 4 [] `shouldBe` []