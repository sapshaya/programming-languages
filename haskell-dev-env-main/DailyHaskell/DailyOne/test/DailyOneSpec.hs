module DailyOneSpec where

import Test.Hspec
import DailyOne

spec :: Spec
spec = do
  describe "quadratic" $ do
    it "produces the quadratic 0" $
      (quadratic 0 0 0 0) `shouldBe` 0

    it "produces the quadratic 3" $
      (quadratic 1 1 1 1) `shouldBe` 3

    it "produces the quadratic 14" $
      (quadratic 2 2 2 2) `shouldBe` 14

  describe "scaleVector" $ do
    it "produces the scale vector of 3 and (3,4)" $
      (scaleVector 3 (3,4)) `shouldBe` (9,12)

    it "produces the scale vector of 5 and (2,5)" $
      (scaleVector 5 (2,5)) `shouldBe` (10,25)

    it "produces the scale vector of 7 and (7,10)" $
      (scaleVector 7 (7,10)) `shouldBe` (49,70)
    
  describe "tripleDistance" $ do
    it "produces the distance of (0,0,0) and (1,8,12)" $
      (tripleDistance (0,0,0)(1,8,12)) `shouldBe` 209

    it "produces the distance of (0,0,0) and (15,6,8)" $
      (tripleDistance (0,0,0)(15,6,8)) `shouldBe` 325

    it "produces the distance of (3,3,3) and (15,19,24)" $
      (tripleDistance (4,4,4) (21,15,17)) `shouldBe` 579
