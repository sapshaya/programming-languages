module DailyOne where

{-
This quadratic function takes four 
double arguments and returns a double(output). It calculates a quadratic 
function using the given formula. ** operator is for exponentiation.
-}
quadratic :: Int -> Int -> Int -> Int -> Int
quadratic a b c x = a + b*x + c*x*x
{-
This scaleVector function takes a 
double and a tuple of two doubles, and it also returns a tuple of two doubles. 
It shows the scale vector by the given factor using the formula s^x s*y.
-}
scaleVector :: Int -> (Int, Int) -> (Int, Int)
scaleVector x (a, b) = (a*x, b*x)
{-
This tripleDistance function takes two tuples of three doubles and returns a double. 
It calculates the distance between two points in 3-dimensional space using the given formula.
-}
tripleDistance :: (Double, Double, Double) -> (Double, Double, Double) -> Double
tripleDistance (x1, y1, z1) (x2, y2, z2) =
 (x2 - x1)^2 + (y2 - y1)^2 + (z2 - z1)^2