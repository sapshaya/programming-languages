module DailySevenSpec where

import Test.Hspec
import DailySeven
import Data.List (intercalate)

spec :: Spec
spec = do
  describe "findLongest" $ do
    it "returns the longest string from a list of strings" $
      findLongest ["apple", "banana", "pear"] `shouldBe` "banana"

    it "returns an empty string for an empty list" $
      findLongest [] `shouldBe` ""

  describe "anyLarger" $ do
    it "returns True if any element in the list is larger than the given number" $
      anyLarger 5 [1, 3, 7, 2, 4] `shouldBe` True

    it "returns False if no element in the list is larger than the given number" $
      anyLarger 10 [1, 2, 3, 4, 5] `shouldBe` False

  describe "allNames" $ do
    it "concatenates the first names and last names with commas" $
      allNames [("John", "Doe"), ("Alice", "Smith"), ("Bob", "Johnson")]
        `shouldBe` "John Doe, Alice Smith, Bob Johnson"

    it "returns an empty string for an empty list" $
      allNames [] `shouldBe` ""
