module DailySeven where

import Data.List (intercalate)

findLongest :: [String] -> String
findLongest [] = ""
findLongest (x:xs)
  | all (\w -> length x >= length w) xs = x
  | otherwise = findLongest xs

anyLarger :: Integer -> [Integer] -> Bool
anyLarger _ [] = False
anyLarger n (x:xs)
  | x >= n = True
  | otherwise = anyLarger n xs

allNames :: [(String, String)] -> String
allNames = intercalate ", " . map (\(firstName, lastName) -> firstName ++ " " ++ lastName)
