module DailyEight where


-- Define the Event data type with the specified fields
data Event = Event
  { name :: String,
    day :: Int,
    month :: String,
    year :: Int,
    xlocation :: Double,
    ylocation :: Double
  }

-- Show instance for Event
instance Show Event where
  show (Event name day month year xlocation ylocation) =
    "Event \"" ++ name ++ "\" " ++ show day ++ " \"" ++ month ++ "\" " ++ show year ++ " " ++ show xlocation ++ " " ++ show ylocation

-- inYear function: Filter events that occurred during a specific year
inYear :: Int -> [Event] -> [Event]
inYear targetYear = filter (\event -> year event == targetYear)

-- inDayRange function: Filter events that occurred between two given days (inclusive) regardless of the month or year
inDayRange :: Int -> Int -> [Event] -> [String]
inDayRange startDay endDay =
  map name . filter (\event -> day event >= startDay && day event <= endDay)

-- inArea function: Filter events that match the given name and occurred within the specified spatial region
inArea ::
  String -> -- Event name to match
  Double -> -- Lower x location
  Double -> -- Upper x location
  Double -> -- Lower y location
  Double -> -- Upper y location
  [Event] -> -- List of events
  [Event]
inArea targetName minX maxX minY maxY =
  filter
    ( \event ->
        name event == targetName
          && xlocation event >= minX
          && xlocation event <= maxX
          && ylocation event >= minY
          && ylocation event <= maxY
    )
