module DailyEightSpec where

import Test.Hspec
import DailyEight

instance Eq Event where
  (Event name1 day1 month1 year1 xlocation1 ylocation1) ==
    (Event name2 day2 month2 year2 xlocation2 ylocation2) =
      name1 == name2 &&
      day1 == day2 &&
      month1 == month2 &&
      year1 == year2 &&
      xlocation1 == xlocation2 &&
      ylocation1 == ylocation2

spec :: Spec
spec = do
  describe "inYear" $ do
    it "returns events that occurred during the specified year" $
      let events =
            [ Event "Event1" 10 "January" 2022 1.0 2.0,
              Event "Event2" 15 "February" 2021 3.0 4.0,
              Event "Event3" 20 "March" 2022 5.0 6.0
            ]
       in inYear 2022 events `shouldBe` [Event "Event1" 10 "January" 2022 1.0 2.0, Event "Event3" 20 "March" 2022 5.0 6.0]

    it "returns an empty list for no events in the specified year" $
      let events =
            [ Event "Event1" 10 "January" 2022 1.0 2.0,
              Event "Event2" 15 "February" 2021 3.0 4.0,
              Event "Event3" 20 "March" 2022 5.0 6.0
            ]
       in inYear 2023 events `shouldBe` []

  describe "inDayRange" $ do
    it "returns names of events that occurred between the specified days" $
      let events =
            [ Event "Event1" 10 "January" 2022 1.0 2.0,
              Event "Event2" 15 "February" 2022 3.0 4.0,
              Event "Event3" 20 "March" 2022 5.0 6.0
            ]
       in inDayRange 12 25 events `shouldBe` ["Event1", "Event2"]

    it "returns an empty list for no events in the specified day range" $
      let events =
            [ Event "Event1" 10 "January" 2022 1.0 2.0,
              Event "Event2" 15 "February" 2022 3.0 4.0,
              Event "Event3" 20 "March" 2022 5.0 6.0
            ]
       in inDayRange 1 5 events `shouldBe` []

  describe "inArea" $ do
    it "returns events that match the given name and occurred within the specified spatial region" $
      let events =
            [ Event "Event1" 10 "January" 2022 1.0 2.0,
              Event "Event2" 15 "February" 2022 3.0 4.0,
              Event "Event3" 20 "March" 2022 5.0 6.0,
              Event "Event4" 25 "April" 2022 1.5 3.5
            ]
       in inArea "Event2" 2.0 4.0 3.0 5.0 events `shouldBe` [Event "Event2" 15 "February" 2022 3.0 4.0]

    it "returns an empty list for no events that match the given name and spatial region" $
      let events =
            [ Event "Event1" 10 "January" 2022 1.0 2.0,
              Event "Event2" 15 "February" 2022 3.0 4.0,
              Event "Event3" 20 "March" 2022 5.0 6.0,
              Event "Event4" 25 "April" 2022 1.5 3.5
            ]
       in inArea "Event5" 2.0 4.0 3.0 5.0 events `shouldBe` []
