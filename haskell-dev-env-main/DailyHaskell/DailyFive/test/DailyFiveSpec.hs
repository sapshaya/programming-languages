module DailyFiveSpec where

import Test.Hspec
import DailyFive 

spec :: Spec  
spec = do 
    describe "multPairs" $ do
        it "multPairs consumes [(3,3), (2,4), (0,2)] " $
            multPairs [(3,3), (2,4), (0,2)] `shouldBe` [9,8,0]
        it "multPairs consumes [(3,2),(0,0)] " $
            multPairs [(3,2),(0,0)] `shouldBe` [6,0]
        it "multPairs consumes [(6,6),(9,9),(1,5),(4,4)]" $
            multPairs [(6,6),(9,9),(1,5),(4,4)] `shouldBe` [36,81,5,16]
    
    describe "squareList" $ do
        it "squareList [1,2,3]" $
            squareList [1,2,3] `shouldBe` [(1,1),(2,4),(3,9)]
        it "squareList [1,5,4,7]" $
            squareList [1,5,4,7] `shouldBe` [(1,1),(5,25),(4,16),(7,49)]
        it "squareList [4,2]" $
            squareList [4,2] `shouldBe` [(4,16),(2,4)]
    
    describe "findLowerCase" $ do
        it "findLowerCase [\"Hello, how are you\"]" $
            findLowercase ["Hello, how are you"] `shouldBe` []
        it "findLowerCase [\"This\", \"Test\"]" $
            findLowercase ["This", "Test"] `shouldBe` []
        it "findLowerCase [\"TEST TEST TEST\"]" $
            findLowercase ["TEST TEST TEST"] `shouldBe` []