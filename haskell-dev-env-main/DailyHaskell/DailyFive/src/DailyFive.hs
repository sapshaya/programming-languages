module DailyFive where 

import Data.Char

{-
This function take a list of tuples that contains two integers 
and it returns a list of its final product. It uses a function called map that multiplies each
pair of ints in the list
-}
multPairs :: [(Int, Int)] -> [Int]
multPairs [] = []
multPairs pairs = map (\(x,y) -> x*y) pairs
{-
This function takes a list of ints and returns a list of tuples that contain int and its square
It uses a function called map that creates a tuple that contains of an int and its square
-}
squareList :: [Int] -> [(Int,Int)]
squareList [] = []
squareList xs = map (\x ->(x, x*x)) xs
{-
This function takes a list of strings and it returns a list of booleans that indicate if the
string starts with a lowercase letter. It uses a function called map that checks if the
first character of every string is lowercase using isLower function from imported module called
Data.Char module
-}
findLowercase :: [String] -> [Bool]
findLowercase [] = []
findLowercase strings = map (\s -> isLower (head s)) strings