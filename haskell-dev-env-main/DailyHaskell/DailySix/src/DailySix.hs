module DailySix where

--removes strings from a list that are shorter than a given length
--  consumes a number and list of strings
--  produces a list of strings

shorterThan :: Int -> [String] -> [String]
shorterThan n = filter (lessThan n)

--helper method to shorterThan, compares string length to a number
lessThan :: Int -> String -> Bool
lessThan n s = length s < n


--removes multiples of a given number from a list of numbers
--  consumes an integer and array of integers
--  produces an array of integers
removeMultiples :: Integer  -> [Integer ] -> [Integer]
removeMultiples n = filter (notMultipleOf n)

--helper method to removeMultiples, determines if a number is not a multiple of a given number
notMultipleOf :: Integer -> Integer -> Bool
notMultipleOf n x = x `mod` n /= 0


--removes nothings from a list of maybes
--  consumes a list of maybe of type a
--  produces a list of maybe of type a
onlyJust :: [Maybe a] -> [Maybe a]
onlyJust = filter notNothing

--helper method to onlyJust, checks if Maybe of type a is the nothing value
notNothing :: Maybe a -> Bool
notNothing Nothing = False
notNothing _ = True