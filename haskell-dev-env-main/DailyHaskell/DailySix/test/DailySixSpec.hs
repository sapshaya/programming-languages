module DailySixSpec where

import Test.Hspec
import DailySix


spec :: Spec
spec = do
    describe "shorterThan" $ do
        it "returns strings shorter than the given length" $
            shorterThan 5 ["Hello", "Test", "World", "Hi"] `shouldBe` (["Test", "Hi"] :: [String])

        it "returns an empty list if no strings are shorter than the given length" $
            shorterThan 10 ["a", "b", "c"] `shouldBe` ([] :: [String])

    describe "removeMultiples" $ do
        it "removes multiples of the given number from the list" $
            removeMultiples 3 [1, 2, 3, 4, 5, 6, 7, 8, 9] `shouldBe` ([1, 2, 4, 5, 7, 8] :: [Integer])

        it "returns an empty list if all numbers are multiples of the given number" $
            removeMultiples 2 [2, 4, 6, 8, 10] `shouldBe` ([] :: [Integer])

    describe "onlyJust" $ do
        it "removes Nothings from the list of Maybes" $
            onlyJust [Just 1, Nothing, Just 2, Nothing, Just 3] `shouldBe` ([Just 1, Just 2, Just 3] :: [Maybe Integer])

        it "returns an empty list if all elements are Nothing" $
            onlyJust [Nothing, Nothing, Nothing] `shouldBe` ([] :: [Maybe Integer])
