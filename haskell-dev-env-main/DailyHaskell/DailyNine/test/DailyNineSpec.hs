module DailyNineSpec where

import Test.Hspec
import DailyNine

spec :: Spec
spec = do
    describe "findSmallest" $ do
        it "returns Nothing for an empty list" $
            findSmallest ([] :: [Int]) `shouldBe` Nothing

        it "returns the smallest element for a non-empty list" $
            findSmallest [4, 2, 9, 5, 1] `shouldBe` Just 1

    describe "allTrue" $ do
        it "returns Nothing for an empty list" $
            allTrue ([] :: [Bool]) `shouldBe` Nothing

        it "returns True if all elements are True" $
            allTrue [True, True, True] `shouldBe` Just True

        it "returns False if any element is False" $
            allTrue [True, False, True] `shouldBe` Just False

    describe "countAllVotes" $ do
        it "returns the correct vote counts for an empty list" $
            countAllVotes ([] :: [Maybe Bool]) `shouldBe` (0, 0, 0)

        it "returns the correct vote counts for a list with only Nothing" $
            countAllVotes [Nothing, Nothing, Nothing] `shouldBe` (3, 0, 0)

        it "returns the correct vote counts for a list with only Just True" $
            countAllVotes [Just True, Just True, Just True] `shouldBe` (0, 3, 0)

        it "returns the correct vote counts for a list with only Just False" $
            countAllVotes [Just False, Just False, Just False] `shouldBe` (0, 0, 3)

        it "returns the correct vote counts for a list with mixed votes" $
            countAllVotes [Nothing, Just True, Just False, Just True, Nothing] `shouldBe` (2, 2, 1)

