module DailyNine where


--finds the smallest element of a list or returns nothing if empty list
--  consumes a list of type a
--  returns a Maybe a
findSmallest :: (Ord a) => [a] -> Maybe a
findSmallest [] = Nothing
findSmallest list = Just (minimum list)


--determines if all booleans in a list are true, or completely empty
--  consumes a list of boolean
--  produces a Maybe Bool
allTrue :: [Bool] -> Maybe Bool
allTrue [] = Nothing
allTrue list = Just (and list)



--determines vote counts from a list of votes
--  consumes a list of Maybe Bool
--  produces a 3-tuple of 3 integers
countAllVotes :: [Maybe Bool] -> (Integer, Integer, Integer)
countAllVotes list = (countNothing list, countYay list, countNay list)

--countAllVotes helper methods

--counts all nothing elements in a Maybe Bool
countNothing :: [Maybe Bool] -> Integer
countNothing list = toInteger (length (filter (== Nothing) list))

--counts all Just True elements in a Maybe Bool
countYay :: [Maybe Bool] -> Integer
countYay list = toInteger (length (filter (== Just True) list))

--counts all Just False elements in a Maybe Bool
countNay :: [Maybe Bool] -> Integer
countNay list = toInteger (length (filter (== Just False) list))