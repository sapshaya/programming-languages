module WeeklyTwo where


data TriTree a = Empty |
                 Leaf a |
                 Node a a (TriTree a) (TriTree a) (TriTree a)
                 deriving (Show, Eq)

-- Determines whether a given value is found in a TriTree. 
-- Takes a value and a TriTree of the same type.
-- Returns a boolean indicating if the value is present in the TriTree.
search :: (Ord a) => a -> TriTree a -> Bool
search x tree = case tree of
    Empty -> False
    Leaf a -> x == a
    Node a1 a2 t1 t2 t3 -> (x == a1 || x == a2) || (search x t1 || search x t2 || search x t3)

-- Inserts a value into the TriTree.
-- Takes a value and a TriTree of the same type.
-- Returns the updated TriTree.
insert :: (Ord a) => a -> TriTree a -> TriTree a
insert x tree = case tree of
    Empty -> Leaf x
    Leaf a -> if a < x
                then Node a x Empty Empty Empty
                else Node x a Empty Empty Empty
    Node a1 a2 t1 t2 t3 -> if x > a2
                            then Node a1 a2 t1 t2 (insert x t3)
                            else if x < a1
                                then Node a1 a2 (insert x t1) t2 t3
                                else Node a1 a2 t1 (insert x t2) t3

-- Inserts a list of values into a TriTree.
-- Takes a list and a TriTree of the same element type.
-- Returns the updated TriTree.
insertList :: (Ord a) => [a] -> TriTree a -> TriTree a
insertList list tree = foldr insert tree list

-- Checks if two TriTrees are identical.
-- Takes two TriTrees as input.
-- Returns a boolean indicating whether the TriTrees are identical.
identical :: Eq a => TriTree a -> TriTree a -> Bool
identical Empty Empty = True
identical Empty (Leaf _) = False
identical (Leaf _) Empty = False
identical (Leaf a) (Leaf b) = a == b
identical Node {} Empty = False
identical Empty Node {} = False
identical (Node a1 a2 ta1 ta2 ta3) (Leaf b) = False
identical (Leaf a) (Node a1 a2 ta1 ta2 ta3) = False
identical (Node a1 a2 ta1 ta2 ta3) (Node b1 b2 tb1 tb2 tb3) =
    if (a1 == b1 || a2 == b2)
        then identical ta1 tb1 ||
            identical ta2 tb2 ||
            identical ta3 tb3
        else False

-- Maps a function across a TriTree.
-- Takes a function and a TriTree of the function input element.
-- Returns a TriTree of elements from the output of the function.
treeMap :: (a -> b) -> TriTree a -> TriTree b
treeMap _ Empty = Empty
treeMap f (Leaf a) = Leaf (f a)
treeMap f (Node a1 a2 t1 t2 t3) = Node (f a1) (f a2) (treeMap f t1) (treeMap f t2) (treeMap f t3)

-- Folds a function on top of the TriTree in a preorder traversal.
-- Takes a function, an initial value, and a TriTree of the same type.
-- Returns the accumulated value after folding.
treeFoldPreOrder :: (a -> a -> a) -> a -> TriTree a -> a
treeFoldPreOrder _ v Empty = v
treeFoldPreOrder f v (Leaf a) = f v a
treeFoldPreOrder f v (Node a1 a2 t1 t2 t3) = treeFoldPreOrder f (treeFoldPreOrder f (treeFoldPreOrder f (f (f v a1) a2) t1) t2) t3

-- Folds a function on top of the TriTree in an inorder traversal.
-- Takes a function, an initial value, and a TriTree of the same type.
-- Returns the accumulated value after folding.
treeFoldInOrder :: (a -> a -> a) -> a -> TriTree a -> a
treeFoldInOrder _ v Empty = v
treeFoldInOrder f v (Leaf a) = f v a
treeFoldInOrder f v (Node a1 a2 t1 t2 t3) = treeFoldInOrder f (f (treeFoldInOrder f (f (treeFoldInOrder f v t1) a1) t2) a2) t3

-- Folds a function on top of the TriTree in a postorder traversal.
-- Takes a function, an initial value, and a TriTree of the same type.
-- Returns the accumulated value after folding.
treeFoldPostOrder :: (a -> a -> a) -> a -> TriTree a -> a
treeFoldPostOrder _ v Empty = v
treeFoldPostOrder f v (Leaf a) = f v a
treeFoldPostOrder f v (Node a1 a2 t1 t2 t3) = f (f (treeFoldPostOrder f (treeFoldPostOrder f (treeFoldPostOrder f v t1) t2) t3) a1) a2
