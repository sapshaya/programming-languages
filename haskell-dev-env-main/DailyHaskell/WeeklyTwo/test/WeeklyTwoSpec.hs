module WeeklyTwoSpec where

import Test.Hspec
import WeeklyTwo


spec :: Spec
spec = do
  describe "search" $ do
    it "search 1" $
        (search 'b' (Node 'a' 'c' (Leaf 'b') (Empty) (Empty))) `shouldBe` True
    it "search 2" $
        (search 8 (Node 1 8 (Empty) (Empty) (Empty))) `shouldBe` True
    it "search 3" $
        (search 6 (Node 1 4 (Empty) (Leaf 6) (Empty))) `shouldBe` True

  describe "insert" $ do
    it "insert 1" $
        (insert 6 (Leaf 1)) `shouldBe` Node 1 6 Empty Empty Empty
    it "insert 2" $
        (insert 3 (Leaf 4)) `shouldBe` Node 3 4 Empty Empty Empty
    it "insert 3" $
        (insert 6 (Node 1 2 (Empty) (Empty) (Empty))) `shouldBe` Node 1 2 Empty Empty (Leaf 6)

  describe "insertList" $ do
    it "insertList 1" $
        (insertList [1,2,3,4,5] Empty) `shouldBe` Node 4 5 (Node 2 3 (Leaf 1) Empty Empty) Empty Empty
    it "insertList 2" $
        (insertList [6, 7, 8] Empty)  `shouldBe` Node 7 8 (Leaf 6) Empty Empty
    it "insertList 3" $
        (insertList [6, 7, 8] (Leaf 9)) `shouldBe` Node 8 9 (Node 6 7 (Empty) (Empty) (Empty)) Empty Empty

  describe "identical" $ do
    it "identical 1" $
        (identical (Empty) (Node 1 2 Empty Empty Empty)) `shouldBe` False
    it "identical 2" $
        (identical (Node 1 2 Empty Empty Empty) (Empty)) `shouldBe` False
    it "identical 3" $
        (identical (Node 1 2 Empty Empty (Leaf 6)) (Node 1 2 Empty Empty (Leaf 6))) `shouldBe` True

  describe "treeMap" $ do
    it "treeMap 1" $
        (treeMap (\x -> (x*3)) (Empty)) `shouldBe` Empty
    it "treeMap 2" $
        (treeMap (\x -> (x*3)) (Node 3 4 (Leaf 2) Empty Empty)) `shouldBe` Node 9 12 (Leaf 6) Empty Empty
    it "treeMap 3" $
        (treeMap (\x -> (x*3)) (Node 4 5 (Node 2 3 (Leaf 1) Empty Empty) Empty Empty)) `shouldBe` Node 12 15 (Node 6 9 (Leaf 3) Empty Empty) Empty Empty

  describe "treeFoldPreOrder" $ do
    it "treeFoldPreOrder 1" $
        (treeFoldPreOrder (\x -> \y -> (x + y)) 6 (Leaf 6)) `shouldBe` 12
    it "treeFoldPreOrder 2" $
        (treeFoldPreOrder (\x -> \y -> (x + y)) 5 (Node 4 5 (Leaf 2) Empty Empty)) `shouldBe` 16
    it "treeFoldPreOrder 3" $
        (treeFoldPreOrder (\x -> \y -> (x + y)) (-1) (Node 12 15 (Node 6 9 (Leaf 3) Empty Empty) Empty Empty)) `shouldBe` 44

  describe "treeFoldInOrder" $ do
    it "treeFoldInOrder 1" $
        (treeFoldInOrder (\x -> \y -> (x + y)) 5 (Node 4 5 (Leaf 2) Empty Empty)) `shouldBe` 16
    it "treeFoldInOrder 2" $
        (treeFoldInOrder (\x -> \y -> (x + y)) 5 (Empty)) `shouldBe` 5
    it "treeFoldInOrder 3" $
        (treeFoldInOrder (\x -> \y -> (x + y)) 6 (Leaf 6)) `shouldBe` 12

  describe "treeFoldPostOrder" $ do
    it "treeFoldPostOrder 1" $
        (treeFoldPostOrder (\x -> \y -> (x + y)) 6 (Leaf 6)) `shouldBe` 12
    it "treeFoldPostOrder 2" $
        (treeFoldPostOrder (\x -> \y -> (x + y)) 5 (Node 4 5 (Leaf 2) Empty Empty)) `shouldBe` 16
    it "treeFoldPostOrder 3" $
        (treeFoldPostOrder (\x -> \y -> (x + y)) (-1) (Node 12 15 (Node 6 9 (Leaf 3) Empty Empty) Empty Empty)) `shouldBe` 44
