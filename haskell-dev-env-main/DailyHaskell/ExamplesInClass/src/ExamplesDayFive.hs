module ExamplesDayFive where
   -- quicksort 

   quicksort :: (Ord a) => [a] -> [a]
   quicksort [] = []
   quicksort (e:es) = let (smaller, larger) = split e es
                        in
                            quicksort smaller ++ [e] ++ quicksort larger 
   -- split for Quicksort
   --COnsume a pivot ( splitting elemt ) and a List of elements
   --Produce a tuple with two sublists: all the elements smaller than the pivot
   --      and all the elements larger than the pivot
   split :: (Ord a) => a -> [a] -> ([a],[a])
   split _ [] = ([], [])
   split p (e:es) = let (first, second) = split p es
                in
                    if e < p 
                       then ( e : first, second )
                       else ( first, e : second )


splitM :: [a] -> ([a],[a])
splitM [] = ([], [])
splitM [e] = ([e], [])
splitM (e:f:elems) = let (first, second) = splitM elems
                        in
                            (e:first, f:second)

splitM' :: [a] -> ([a], [a])
splitM' [] = ([], [])
splitM' theList = let num = (length theList) `div` 2
                        in
                            (take num theList, drop num theList)

mergesort :: (Ord a) => [a] -> [a]
mergesort [] = []
merge theList = let (firsthalf, secondhalf) = splitM theList 
                        in
                            merge (mergesort firsthalf) (mergesort secondhalf)
merge ::(Ord a) => [a] -> [a] -> [a]
merge [] [] = []
merge listOne [] = listOne 
merge [] listTwo = listTwo
merge (e:es) (f:fs) = if e < f
                        then e : merge es f:fs 