module ExamplesApril17 where

{- 

map function

    that applies a function

    f to all the elements in

Some structure:

    map ::(a→b)→[a]→[b]

    map (\x→x+2)[1,2,3]

    ⇒[3,4,5] 

myMap f [] = []
myMap f (x:xs) = (f x):myMap f xs

Category Theory
    Functor
-Filter Predicate
-Fold
    foldl(+) 0 [1,2,3,4] plus from left to right (0+1,0+2,0+3,0+4)
    foldr(+) 0 [1,2,3,4] plus from right to left (0+4,0+3,0+2,0+1)
    
-} 